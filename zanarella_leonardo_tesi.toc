\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {chapter}{Introduzione}{i}
\contentsline {chapter}{\numberline {1}Recommendation System}{1}
\contentsline {section}{\numberline {1.1}Definizione di Recommendation System}{1}
\contentsline {subsection}{\numberline {1.1.1}Scopo del Recommendation System}{3}
\contentsline {subsection}{\numberline {1.1.2}Acquisizione informazioni }{3}
\contentsline {section}{\numberline {1.2}Storia di Recommendation System}{4}
\contentsline {subsection}{\numberline {1.2.1}Recommendation System Collaborative Filtering }{4}
\contentsline {subsection}{\numberline {1.2.2}Recommendation System Content-based Filtering }{7}
\contentsline {subsection}{\numberline {1.2.3}Recommendation System Hybrid Filtering }{11}
\contentsline {section}{\numberline {1.3}Stato dell Arte del Recommendation System}{14}
\contentsline {subsection}{\numberline {1.3.1}Recommendation System Use Case}{14}
\contentsline {section}{\numberline {1.4}Conclusioni}{16}
\contentsline {chapter}{\numberline {2}Apache Mahout}{19}
\contentsline {section}{\numberline {2.1}Machine Learning}{19}
\contentsline {subsection}{\numberline {2.1.1}Machine Learning in Apache Mahout}{21}
\contentsline {section}{\numberline {2.2}Content-based Filtering in Mahout}{23}
\contentsline {subsection}{\numberline {2.2.1}Esecuzione Recommendation}{23}
\contentsline {subsection}{\numberline {2.2.2}Valutazione Recommendation}{26}
\contentsline {section}{\numberline {2.3}Sviluppi Futuri}{27}
\contentsline {section}{\numberline {2.4}Conclusioni}{28}
\contentsline {chapter}{\numberline {3}LockSpot - change your LockScreen}{29}
\contentsline {section}{\numberline {3.1}LockSpot in Sintesi}{31}
\contentsline {section}{\numberline {3.2}Premi e Riconoscimenti}{31}
\contentsline {chapter}{\numberline {4}LockSpot - Implementazione e Struttura}{33}
\contentsline {section}{\numberline {4.1}LockSpot Client B2C}{34}
\contentsline {section}{\numberline {4.2}LockSpot Client B2B}{35}
\contentsline {section}{\numberline {4.3}LockSpot Server }{38}
\contentsline {chapter}{\numberline {5}LockSpot Valutazione di Prestazioni}{49}
\contentsline {section}{\numberline {5.1}Correlazioni con et\`a e sesso}{55}
\contentsline {section}{\numberline {5.2}Valutazioni finali}{58}
\contentsline {chapter}{Conclusioni}{63}
\contentsline {section}{\numberline {5.3}LockSpot - Sviluppi futuri }{64}
\contentsline {chapter}{Bibliografia}{67}
\contentsline {chapter}{{Ringraziamenti}}{73}
